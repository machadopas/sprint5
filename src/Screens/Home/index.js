import React, { useState } from 'react';
import useLocation from '../../Hooks/useLocation';
import MapView, { Marker } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { marcadores } from '../../utils/Markers';
import { SafeAreaView } from 'react-native-safe-area-context';
import { View, StyleSheet, Text } from 'react-native';
import { Button, TextInput } from "react-native-paper";
import Formulario from '../../Components/Formulário';

export default function HomeScreen(mark) {

    const [lugar, novoLugar] = useState()

    console.log(lugar)

    const [destino, setDestino] = useState(null);

    const [form, popForm] = useState(false);

    const { coords, errorMsg } = useLocation();

    const [marker, setMarker] = useState(marcadores);

    function novoMarcador(coordinate) {
        setMarker([...marker, coordinate])

    }
    console.log(marker)

    const [nome, novoNome] = useState("")

    const [desc, novoDesc] = useState("")

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <MapView
                onPress={(clique) => {
                    popForm(!form)
                    novoLugar({
                        latitude: clique.nativeEvent.coordinate.latitude,
                        longitude: clique.nativeEvent.coordinate.longitude
                    })
                }}
                onPanDrag={() => { popForm(false) }}
                showsUserLocation
                style={{
                    height: '100%',
                    width: '100%',
                    position: 'absolute',
                }}
            >
                {
                    marker.map((m) => {
                        return <Marker
                            coordinate={m}
                            key={Math.random().toString()}
                            onPress={() => setDestino(m)}
                            title={`${nome}`}
                            description={`${desc}`}
                        />
                    })}
                {
                    <MapViewDirections
                        strokeWidth={4}
                        origin={coords}
                        destination={destino}
                        apikey='AIzaSyD5PklHsWM9nz13Sqtwzy8N71Hg3mXPEMQ'

                    />
                }
            </MapView>
            {
                form ? (
                    <View style={styles.cartao}>
                        <Text style={styles.topo}>NOVO LOCAL</Text>
                        <View style={styles.info}>
                            <Text style={styles.titulo}>NOME DO LOCAL</Text>
                            <TextInput
                                style={styles.nome}
                                placeholder="Nome"
                                onChangeText={(topo) => novoNome(topo)}
                            />
                            <Text style={styles.titulo}>DESCRIÇÃO</Text>
                            <TextInput
                                style={styles.descricao}
                                placeholder="Descrição"
                                onChangeText={(base) => novoDesc(base)}>
                            </TextInput>
                            <Button
                                mode="contained" style={styles.botao}
                                onPress={() => {
                                    popForm(!form),
                                        novoMarcador(
                                            {
                                                title: nome,
                                                description: desc,
                                                ...lugar
                                            }
                                        )
                                }}
                            >CRIAR</Button>
                        </View>
                    </View >
                ) : null
            }
        </SafeAreaView >
    )

}

const styles = StyleSheet.create({
    cartao: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: "#f0f0f0",
        margin: 200,
        marginVertical: 200,
        marginHorizontal: 20,
        borderRadius: 8
    },
    topo: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#000000',
        marginVertical: 30,

    },
    info: {
        marginBottom: 70,
        justifyContent: 'center',
        alignContent: 'center'

    },
    titulo: {
        fontSize: 18,
        color: "#000000"
    },
    nome: {
        width: 280
    },
    descricao: {
        height: 120
    },
    botao: {
        marginVertical: 30,
        borderRadius: 8

    }


})