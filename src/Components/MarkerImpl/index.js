import React from 'react';
import { Marker } from 'react-native-maps';

export default function MarkerImpl({
    onPress,
    mark,
}) {
    return (
        <Marker
            onPress={onPress}
            tracksViewChanges={false}
            key={mark._id}
            coordinate={{
                latitude: mark.latitude,
                longitude: mark.longitude
            }}
            title={mark.title}
        >
        </Marker>
    );
}
