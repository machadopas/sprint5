import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';

import HomeScreen from './Screens/Home';

function App() {
  return <HomeScreen />
}

const estilos = StyleSheet.create({
  tela: {
    flex: 1,
  }
})

export default App;