import React, { useState } from "react";
import { markers } from "../../utils/Markers";
import { View, StyleSheet, Text } from "react-native";
import { Button, TextInput } from "react-native-paper";
import { marcadores } from "../../utils/Markers";

export default function Formulario() {
    const [nome, setNome] = useState()
    const [descrição, setDescrição] = useState()

    const [lugar, novoLugar] = useState([])

    const [n, novoNome] = useState("")
    const [desc, novoDesc] = useState("")

    const [marker, setMarker] = useState(marcadores);

    console.log(n)
    console.log(desc)

    function novoMarcador(coordinate) {
        setMarker([...marker, coordinate])
    }

    return (
        <View style={styles.cartao}>
            <Text style={styles.topo}>NOVO LOCAL</Text>
            <View style={styles.info}>
                <Text style={styles.titulo}>NOME DO LOCAL</Text>
                <TextInput
                    style={styles.nome}
                    placeholder="Nome"
                    onChangeText={(text) => novoNome(text)}
                />
                <Text style={styles.titulo}>DESCRIÇÃO</Text>
                <TextInput
                    style={styles.descricao}
                    placeholder="Descrição"
                    onChangeText={(text) => novoDesc(text)}>
                </TextInput>
                <Button
                    mode="contained" style={styles.botao}
                    onPress={(clique) => {
                    }}
                >CRIAR</Button>
            </View>
        </View >
    );
}

const styles = StyleSheet.create({
    cartao: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: "#f0f0f0",
        margin: 200,
        marginVertical: 200,
        marginHorizontal: 20,
        borderRadius: 8
    },
    topo: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#000000',
        marginVertical: 30,

    },
    info: {
        marginBottom: 70,
        justifyContent: 'center',
        alignContent: 'center'

    },
    titulo: {
        fontSize: 18,
        color: "#000000"
    },
    nome: {
        width: 280
    },
    descricao: {
        height: 120
    },
    botao: {
        marginVertical: 30,
        borderRadius: 8

    }


});